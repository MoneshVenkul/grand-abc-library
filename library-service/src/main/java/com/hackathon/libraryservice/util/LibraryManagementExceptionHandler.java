package com.hackathon.libraryservice.util;

import com.hackathon.libraryservice.exceptions.*;

import feign.FeignException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@RequestMapping(produces = "application/vnd.error+json")
public class LibraryManagementExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoSuchUserException.class)
    protected ResponseEntity<ErrorDto> handleWhileUserNotFound(NoSuchUserException e){

        return new ResponseEntity<>(ErrorDto
                .builder()
                .errorCode(404)
                .errorMessage(e.getMessage())
                .build(), HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(BookAlreadyBorrowedException.class)
    protected ResponseEntity<ErrorDto> handleWhileUserNotFound(BookAlreadyBorrowedException e){

        return new ResponseEntity<>(ErrorDto
                .builder()
                .errorCode(409)
                .errorMessage(e.getMessage())
                .build(), HttpStatus.CONFLICT);
    }
    @ExceptionHandler(BookNotFoundException.class)
    protected ResponseEntity<ErrorDto> handleWhileUserNotFound(BookNotFoundException e){

        return new ResponseEntity<>(ErrorDto
                .builder()
                .errorCode(404)
                .errorMessage(e.getMessage())
                .build(), HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(DuplicateEntryException.class)
    protected ResponseEntity<ErrorDto> handleWhileUserNotFound(DuplicateEntryException e){

        return new ResponseEntity<>(ErrorDto
                .builder()
                .errorCode(409)
                .errorMessage(e.getMessage())
                .build(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(BorrowedBookNotFoundException.class)
    protected ResponseEntity<ErrorDto> handleWhileBorrowedBookNotFound(BorrowedBookNotFoundException e){

        return new ResponseEntity<>(ErrorDto
                .builder()
                .errorCode(404)
                .errorMessage(e.getMessage())
                .build(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<FieldErrorDto> handleWhileValidating(ConstraintViolationException e){

        List<String> errors = new ArrayList<>();
        for (ConstraintViolation<?> error : e.getConstraintViolations()) {
            errors.add(error.getMessage());
        }
        return new ResponseEntity<>(FieldErrorDto
                .builder()
                .errorCode(500)
                .errorMessage("Field error")
                .errors(errors)
                .build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(FeignException.class)
    protected ResponseEntity<String> handleWhenErrorInFundTransfer(FeignException e){
        return new ResponseEntity<>(e.contentUTF8(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InvalidCredentialsException.class)
    protected ResponseEntity<ErrorDto> handleInvalidCredentialsException(InvalidCredentialsException ex) {
    	ErrorDto dto = new ErrorDto(401, ex.getMessage());
    	return new ResponseEntity<>(dto, HttpStatus.UNAUTHORIZED);
    }
    
    @Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> details = ex.getBindingResult().getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
		FieldErrorDto errors = new FieldErrorDto(400, "Validation failed", details);
		return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
	}
}