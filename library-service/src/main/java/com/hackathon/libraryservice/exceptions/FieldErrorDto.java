package com.hackathon.libraryservice.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
public class FieldErrorDto {
	private int errorCode;
	private String errorMessage;
	private List<String> errors;

}
