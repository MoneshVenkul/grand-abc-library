package com.hackathon.libraryservice.exceptions;

public class BorrowedBookNotFoundException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    public BorrowedBookNotFoundException(String message) {
        super(message);
    }
}
