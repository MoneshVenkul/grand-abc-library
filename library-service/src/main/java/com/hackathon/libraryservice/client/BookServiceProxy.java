package com.hackathon.libraryservice.client;

import com.hackathon.libraryservice.dto.BookDto;
import com.hackathon.libraryservice.dto.Status;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;

@FeignClient(name = "http://BOOK-SERVICE/book-service")
public interface BookServiceProxy {

    @PutMapping("/{ids}/{status}")
    public ResponseEntity<String> updateBookByIds(@PathVariable("ids") List<Long> ids,
                                                  @PathVariable("status") Status status);
    @GetMapping("/{ids}")
    public ResponseEntity<List<BookDto>> getBooks(@PathVariable List<Long> ids);

}
