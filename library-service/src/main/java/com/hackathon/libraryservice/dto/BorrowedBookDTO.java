package com.hackathon.libraryservice.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.hackathon.libraryservice.entity.UserDetail;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import java.time.LocalDate;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class BorrowedBookDTO {
    private UserDetail userDetail;
    private long bookId;
    private LocalDate borrowedOn;
    private LocalDate dueDate;
    private LocalDate returnedOn;
    private double rentAmount;

}
