package com.hackathon.libraryservice.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LoginDto {
	
	@NotNull(message = "Username can't be empty")
    private String username;
	
	@NotNull(message = "Password can't be empty")
	@Size(min = 6, message = "Password should have atleast 6 characters")
    private String password;
}
