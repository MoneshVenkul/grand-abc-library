package com.hackathon.libraryservice.dto;

public enum PAGE {
    PAGE_ONE(0),
    PAGE_TWO(1),
    PAGE_THREE(2),
    PAGE_FOUR(3);

    private final int value;

    PAGE(final int newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }
}