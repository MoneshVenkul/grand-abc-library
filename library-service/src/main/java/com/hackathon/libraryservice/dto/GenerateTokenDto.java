package com.hackathon.libraryservice.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class GenerateTokenDto {
    private String email;
    private String password;
}
