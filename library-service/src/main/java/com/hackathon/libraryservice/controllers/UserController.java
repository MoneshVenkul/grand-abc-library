package com.hackathon.libraryservice.controllers;

import com.hackathon.libraryservice.dto.BorrowBookRequestDto;
import com.hackathon.libraryservice.dto.BorrowBookResponseDto;
import com.hackathon.libraryservice.security.JwtTokenUtil;
import com.hackathon.libraryservice.service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
@Slf4j
@RequiredArgsConstructor
@PreAuthorize("hasRole('ROLE_USER')")
@RequestMapping("/api/v1/users")
public class UserController {
    @Autowired
    private final UserService userService;
    @Autowired
    private final HttpServletRequest request;
    @Autowired
    private final JwtTokenUtil jwtTokenUtil;

    @PostMapping("/orders")
    public ResponseEntity<BorrowBookResponseDto> borrowBooks(@RequestBody BorrowBookRequestDto requestDto) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        int userId = jwtTokenUtil.getUserId(token);
        return new ResponseEntity<>(userService.borrowBooks(requestDto, (long) userId), OK);
    }
}
