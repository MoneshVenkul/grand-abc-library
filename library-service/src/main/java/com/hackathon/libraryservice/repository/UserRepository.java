package com.hackathon.libraryservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hackathon.libraryservice.entity.UserDetail;

@Repository
public interface UserRepository extends JpaRepository<UserDetail,Long> {

	public List<UserDetail> findByUsername(String username);

}
