package com.hackathon.libraryservice.repository;

import com.hackathon.libraryservice.entity.BookBorrower;
import com.hackathon.libraryservice.entity.UserDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookBorrowerRepository extends JpaRepository<BookBorrower,Long> {
    List<BookBorrower> findAllByUserDetail(UserDetail userDetail);
}
