package com.hackathon.libraryservice.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class LibraryUser extends User{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final long userId;

	public LibraryUser(String username, String password,long userId, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.userId = userId;
	}
}
