package com.hackathon.libraryservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Builder
@Data
@Entity
@Table(name = "book_borrower")
public class BookBorrower {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private UserDetail userDetail;
    @Column(name = "book_id",unique = true)
    private long bookId;
    @Column(name = "borrowed_on")
    private LocalDate borrowedOn;
    @Column(name = "due_date")
    private LocalDate dueDate;
    @Column(name = "returned_on")
    private LocalDate returnedOn;
    @Column(name = "rent_amount")
    private double rentAmount;

    public BookBorrower() {
    }

    public BookBorrower(long id, UserDetail userDetail, long bookId,
                        LocalDate borrowedOn, LocalDate dueDate, LocalDate returnedOn, double rentAmount) {
        this.id = id;
        this.userDetail = userDetail;
        this.bookId = bookId;
        this.borrowedOn = borrowedOn;
        this.dueDate = dueDate;
        this.returnedOn = returnedOn;
        this.rentAmount = rentAmount;
    }
}
