package com.hackathon.libraryservice.service;

import com.hackathon.libraryservice.client.BookServiceProxy;
import com.hackathon.libraryservice.dto.*;
import com.hackathon.libraryservice.entity.BookBorrower;
import com.hackathon.libraryservice.entity.UserDetail;
import com.hackathon.libraryservice.exceptions.*;
import com.hackathon.libraryservice.repository.BookBorrowerRepository;
import com.hackathon.libraryservice.repository.UserRepository;
import com.hackathon.libraryservice.security.LibraryUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

import static com.hackathon.libraryservice.util.Error.*;


@Service(value = "userService")
@Slf4j
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private final BookBorrowerRepository bookBorrowerRepository;

    @Autowired
    private final BookServiceProxy bookServiceProxy;


    @Override
    public UserDetails loadUserByUsername(String username) {
        List<UserDetail> userList = userRepository.findByUsername(username);
        if (userList.isEmpty()) {
            log.error("Invalid username or password for user {}", username);
            throw new InvalidCredentialsException("Invalid username or password");
        } else {
            UserDetail user = userList.stream().findFirst().get();
            List<SimpleGrantedAuthority> authorityList = Arrays.asList(new SimpleGrantedAuthority(user.getUserRole().getRoleName()));
            return new LibraryUser(user.getUsername(), user.getPassword(), user.getId(), authorityList);
        }
    }


    public List<BorrowedBookDTO> getBorrowedBooks(Long Id, int page) {
        Optional<UserDetail> userDetail = userRepository.findById(Id);
        if (!userDetail.isPresent()) {
            throw new NoSuchUserException("User cannot be found");
        }
        Pageable pageWithFiveDetails = PageRequest.of(page, 5);
        List<BookBorrower> bookBorrowers = bookBorrowerRepository.findAllByUserDetail(userDetail, pageWithFiveDetails);
        if (bookBorrowers.isEmpty())
            throw new BorrowedBookNotFoundException("No more borrowed books found for this User, We are sorry");
        List<BorrowedBookDTO> borrowedBookDTOs = new ArrayList<>();

        for (BookBorrower bookBorrower : bookBorrowers) {

            borrowedBookDTOs.add(BorrowedBookDTO.builder()
                    .bookId(bookBorrower.getBookId())
                    .borrowedOn(bookBorrower.getBorrowedOn())
                    .dueDate(bookBorrower.getDueDate())
                    .rentAmount(bookBorrower.getRentAmount())
                    .returnedOn(bookBorrower.getReturnedOn())
                    .build());
        }
        return borrowedBookDTOs;
    }


    public BorrowBookResponseDto borrowBooks(BorrowBookRequestDto requestDto, long userId) {
        Optional<UserDetail> userDetail = userRepository.findById(userId);
        if (!userDetail.isPresent())
            throw new NoSuchUserException(USER_NOT_FOUND.getErrorMessage());
        UserDetail detail = userDetail.get();
        List<BookDto> bookDtos = bookServiceProxy.getBooks(requestDto.getBookIds()).getBody();
        if (bookDtos.isEmpty())
            throw new BookNotFoundException(BOOK_NOT_FOUND.getErrorMessage());
        for (BookDto bookDto : bookDtos) {
            if (bookDto.getStatus().equals(Status.BORROWED.name()))
                throw new BookAlreadyBorrowedException(BOOK_ALREADY_BORROWED.getErrorMessage());
            if (bookBorrowerRepository.findAllByUserDetail(detail).stream().anyMatch(bookBorrower -> bookDto.getBookId() == bookBorrower.getBookId()))
                throw new DuplicateEntryException(BOOK_ALREADY_BORROWED_BY_USER.getErrorMessage());
            BookBorrower bookBorrower = BookBorrower.builder()
                    .borrowedOn(LocalDate.now())
                    .dueDate(LocalDate.now().plusDays(14))
                    .userDetail(detail)
                    .bookId(bookDto.getBookId()).build();
            bookBorrowerRepository.save(bookBorrower);
        }
        bookServiceProxy.updateBookByIds(requestDto.getBookIds(), Status.BORROWED);
        return BorrowBookResponseDto.builder()
                .message("You have borrowed the below books")
                .statusCode(200)
                .returnDate(LocalDate.now().plusDays(14))
                .bookDtos(bookDtos)
                .build();
    }


}