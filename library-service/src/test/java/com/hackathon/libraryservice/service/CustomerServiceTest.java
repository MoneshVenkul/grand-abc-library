package com.hackathon.libraryservice.service;

import com.hackathon.libraryservice.client.BookServiceProxy;
import com.hackathon.libraryservice.dto.BookDto;
import com.hackathon.libraryservice.dto.BorrowBookRequestDto;
import com.hackathon.libraryservice.dto.BorrowBookResponseDto;
import com.hackathon.libraryservice.dto.LoginDto;
import com.hackathon.libraryservice.dto.LoginResponseDto;
import com.hackathon.libraryservice.entity.BookBorrower;
import com.hackathon.libraryservice.entity.UserDetail;
import com.hackathon.libraryservice.exceptions.NoSuchUserException;
import com.hackathon.libraryservice.repository.BookBorrowerRepository;
import com.hackathon.libraryservice.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

    @InjectMocks
    private UserService customerService;
    @Mock
    private UserRepository customerRepository;
    @Mock
    private BookServiceProxy bookServiceProxy;
    @Mock
    private BookBorrowerRepository bookBorrowerRepository;

    @Test
    public void testBorrowBooks() {
        when(customerRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(new UserDetail()));
        when(bookServiceProxy.getBooks(any()))
                .thenReturn(new ResponseEntity<>(Collections.singletonList(BookDto.builder().status("status").bookId(1L).build()), HttpStatus.OK));
        when(bookBorrowerRepository.findAllByUserDetail(any(UserDetail.class)))
                .thenReturn(Collections.singletonList(new BookBorrower()));
        BorrowBookResponseDto response = customerService.borrowBooks(new BorrowBookRequestDto(), 1L);
        assertNotNull(response);
        assertEquals(response.getStatusCode(),200);
    }
    @Test
    public void testNegativeCase() {
        when(customerRepository.findById(any(Long.class)))
                .thenReturn(Optional.empty());
        assertThrows(NoSuchUserException.class,()-> customerService.borrowBooks(new BorrowBookRequestDto(), 1L));

    }


}
