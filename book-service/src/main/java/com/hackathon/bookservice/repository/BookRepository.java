package com.hackathon.bookservice.repository;

import com.hackathon.bookservice.entity.BookDetail;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<BookDetail, Long> {

	/**
	 * Return list of book based on the or condition
	 * 
	 * @return
	 */
	// public Page<BookDetail>
	// findByBookTitleOrBookAuthorOrStatusOrBookCategory(Pageable pageable);

	/**
	 * Return list of book based on the or condition
	 * 
	 * @return
	 */
	@Query(value = "SELECT * FROM book_detail bookDtl WHERE bookDtl.book_author like ?1% or "
			+ "bookDtl.book_title  like ?1%   or bookDtl.status  like ?1%  or bookDtl.book_category  like ?1% ", 
			countQuery = "SELECT count(*) FROM book_detail bookDtl ", nativeQuery = true)
	public Page<BookDetail> findByBookTitleOrBookAuthorOrStatusOrBookCategory(String search, Pageable page);
}
