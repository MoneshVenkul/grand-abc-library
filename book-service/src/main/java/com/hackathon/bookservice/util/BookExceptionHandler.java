package com.hackathon.bookservice.util;

import java.util.ArrayList;
import java.util.List;

import com.hackathon.bookservice.exceptions.BookNotFoundException;
import com.hackathon.bookservice.exceptions.ErrorDto;
import com.hackathon.bookservice.exceptions.FieldErrorDto;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RequestMapping(produces = "application/vnd.error+json")
public class BookExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ConstraintViolationException.class)
	protected ResponseEntity<FieldErrorDto> handleWhileValidating(ConstraintViolationException e) {

		List<String> errors = new ArrayList<>();
		for (ConstraintViolation<?> error : e.getConstraintViolations()) {
			errors.add(error.getMessage());
		}
		return new ResponseEntity<>(
				FieldErrorDto.builder().errorCode(500).errorMessage("Field error").errors(errors).build(),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(BookNotFoundException.class)
	protected ResponseEntity<ErrorDto> handleWhileUserNotFound(BookNotFoundException e) {
		return new ResponseEntity<>(ErrorDto.builder().errorCode(404).errorMessage(e.getMessage()).build(),
				HttpStatus.NOT_FOUND);
	}
}