package com.hackathon.bookservice.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@ToString
@Getter
public enum Error {
    BOOK_NOT_FOUND(404,"Book cannot be found");
    private final int errorCode;
    private final String errorMessage;
}
