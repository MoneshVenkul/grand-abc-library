package com.hackathon.bookservice.controllers;

import static org.springframework.http.HttpStatus.OK;

import java.awt.print.Book;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.bookservice.dto.BookDto;
import com.hackathon.bookservice.dto.Status;
import com.hackathon.bookservice.exceptions.FieldErrorDto;
import com.hackathon.bookservice.service.BookService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/book-service")
@RequiredArgsConstructor
@PreAuthorize("hasRole('ROLE_USER')")
public class BookController {
	
	/**
	 * 
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(BookController.class);
	
	/**
	 * 
	 */
    @Autowired
    private final BookService bookService;

    @PutMapping("/{ids}/{status}")
    public ResponseEntity<String> updateBookByIds(@PathVariable("ids") List<Long> ids,
                                                  @PathVariable("status") Status status){
        return new ResponseEntity<>(bookService.updateBookByIds(ids,status),OK);

    }
    @GetMapping("/{ids}")
    public ResponseEntity<List<BookDto>> getBooks(@PathVariable List<Long> ids){
        return new ResponseEntity<>(bookService.getBooks(ids),OK);
    }

	

	
	/**
	 * Accept the customer id and returns list of favorite accounts from database.
	 * 
	 * @param page - Current page number for pagination
	 * @return {List<FavouriteAccount>} - List of Favorite accounts.
	 */
	@ApiOperation(value = "Get list of Books.", response = Book.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "Successfull operation", response = BookDto.class),
			@ApiResponse(code = 404, message = "Favourites not found", response = FieldErrorDto.class) })
	@GetMapping("")
	public List<BookDto> getBooks(@RequestParam("search") Optional<String> searhCriteria,
			@RequestParam(defaultValue = "0") Integer offset, @RequestParam(defaultValue = "5") Integer pageSize,
			@RequestParam(defaultValue = "author") String sortBy) {
		LOGGER.info("Getting Books.");
		return bookService.getBooks(searhCriteria,offset, pageSize, sortBy);
	}
}
