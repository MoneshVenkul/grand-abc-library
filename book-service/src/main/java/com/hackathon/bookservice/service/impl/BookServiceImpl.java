package com.hackathon.bookservice.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.hackathon.bookservice.dto.BookDto;
import com.hackathon.bookservice.entity.BookDetail;
import com.hackathon.bookservice.exceptions.BookNotFoundException;
import com.hackathon.bookservice.repository.BookRepository;
import com.hackathon.bookservice.service.BookService;

/**
 * 
 * @author Team-1
 *
 */
@Service
public class BookServiceImpl {

	/**
	 * 
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(BookServiceImpl.class);

	/**
	 * 
	 */
	@Autowired
	private BookRepository bookRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hackathon.bookservice.service.BookService#getBooks(java.lang.Integer,
	 * java.lang.Integer, java.lang.String)
	 */	
	public List<BookDto> getBooks(Optional<String> search, Integer pageNo, Integer pageSize, String sortBy) {
		LOGGER.info("Getting data.");
		PageRequest page = null;
		List<BookDto> resultBooks = new ArrayList<>();
		if (search.isPresent()) {
			page = PageRequest.of(pageNo, pageSize, Sort.by(mapDBColumn(sortBy)));
		} else {
			page = PageRequest.of(pageNo, pageSize, Sort.by(mapToEntity(sortBy)));
		}
		Page<BookDetail> books = getResults(search, page);
		books.forEach(book -> resultBooks.add(mapToDto(book)));
		LOGGER.debug("Total number of books {}", resultBooks.size());
		return resultBooks;
	}

	/**
	 * Executes the query and return the result.
	 * 
	 * @param search
	 * @param page
	 * @return
	 */
	private Page<BookDetail> getResults(Optional<String> search, PageRequest page) {
		Page<BookDetail> books = null;
		if (search.isPresent()) {
			LOGGER.info("--calling find by.");
			books = bookRepository.findByBookTitleOrBookAuthorOrStatusOrBookCategory(search.get(), page);
		} else {
			books = bookRepository.findAll(page);
		}
		if (books.isEmpty()) {
			throw new BookNotFoundException("Book not available at this moment.");
		}
		return books;
	}

	/**
	 * 
	 * @param bookDetail
	 * @return
	 */
	private BookDto mapToDto(BookDetail bookDetail) {
		return BookDto.builder().bookTitle(bookDetail.getBookTitle()).bookAuthor(bookDetail.getBookAuthor())
				.bookCategory(bookDetail.getBookCategory()).status(bookDetail.getStatus()).build();
	}

	/**
	 * 
	 * @param sortValue
	 * @return
	 */
	private String mapDBColumn(String sortValue) {
		String sortColumnName = "book_author";
		if ("status".equalsIgnoreCase(sortValue)) {
			sortColumnName = "status";
		} else if ("title".equalsIgnoreCase(sortValue)) {
			sortColumnName = "book_title";
		} else if ("category".equalsIgnoreCase(sortValue)) {
			sortColumnName = "book_category";
		}
		return sortColumnName;
	}

	/**
	 * 
	 * @param sortValue
	 * @return
	 */
	private String mapToEntity(String sortValue) {
		String sortColumnName = "bookAuthor";
		if ("status".equalsIgnoreCase(sortValue)) {
			sortColumnName = "status";
		} else if ("title".equalsIgnoreCase(sortValue)) {
			sortColumnName = "bookTitle";
		} else if ("category".equalsIgnoreCase(sortValue)) {
			sortColumnName = "bookCategory";
		}
		return sortColumnName;
	}
}
