package com.hackathon.bookservice.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class BookDto {
    private long bookId;
    private String bookTitle;
    private String status;
    private String bookAuthor;
    private String bookCategory;
}
