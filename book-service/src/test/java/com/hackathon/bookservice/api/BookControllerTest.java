/**
 * 
 */
package com.hackathon.bookservice.api;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.hackathon.bookservice.BookServiceApplication;
import com.hackathon.bookservice.controllers.BookController;
import com.hackathon.bookservice.dto.BookDTO;
import com.hackathon.bookservice.service.BookService;


@ExtendWith(SpringExtension.class)
@WebMvcTest(value = BookController.class)
@ContextConfiguration(classes = BookServiceApplication.class)
public class BookControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private BookService bookService;	
	
	@Test
	void testGetBooks() throws Exception{		
		when(bookService.getBooks(Optional.of(""), 0, 5, "author")).thenReturn(getBookData());
		mockMvc.perform(get("/api/v1/book-service?search=&offset=0&pageSize=5&sortBy=author"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$[0].author", Matchers.is("Robin Sharma")));
	}
	
	/**
	 * 
	 * @return
	 */
	private List<BookDTO> getBookData() {
		List<BookDTO> result = new ArrayList<BookDTO>();
		result.add(new BookDTO("Monk who sold his farrery", "Robin Sharma", "Adventure", "Available"));
		result.add(new BookDTO("Half Girl friend", "Cheetan Bhagat", "Romantic", "Available"));
		result.add(new BookDTO("Think and grow rich", "Neppolen Hills", "Motivational", "Not Available"));
		return result;
	}
}
